# diy-consumercontroller

A usb consumer control using [`framework` for CircuitPython](https://gitlab.com/c8767/framework). For more information about how this project works, please refer to the `framework` documentation.

This has been developed for and tested on RP2040, SAMD51, and SAMD21 based microcontroller boards. It has not been tested on Linux devices.

# Getting Started

This guide assumes you have a [CircuitPython compatible board](https://circuitpython.org/downloads) and have followed the [Welcome To CircuitPython](https://learn.adafruit.com/welcome-to-circuitpython) guide to get started. `framework` for CircuitPython uses `asyncio` which requires CircuitPython >=7.1.0

## Installing

**WARNING:** This will overwrite your CIRCUITPY files. Back up your files before starting!

Clone this repository or download this repository as a zip file and unzip it in a convenient location. Copy the contents of the `CIRCUITPY` folder to your CIRCUITPY drive.

Your CIRCUITPY drive should look like this:

```
| CIRCUITPY
|-- boot.py
|-- code.py
|-- lib
|---- asyncio
|---- adafruit_hid
|---- adafruit_ticks.mpy
|---- framework.py
|---- frk_buttons.py
|---- frk_digitalout.py
|---- frk_incrementalencoder.py
|---- frk_oneshot
|---- frk_usbconsumercontrol
|-- usb_cc
|---- usb_cc.json
|---- usb_cc.py
```

## Configuring

The main configuration point is the [`usb_cc.json` file](https://gitlab.com/c8767/diy-consumercontroller/-/blob/main/CIRCUITPY/usb_cc/usb_cc.json). It contains almost all of the information needed to get up and running. Especially of note is the pin names (eg. `GP0`, `GP1`, etc. These are also compatible with other naming schemes: `D0`, `A0`, etc. depending on your board) and `buttons` pin aliases. Change the pins names to match your board/peripheral configuration. Change the pin aliases to the functionality you want from each button. These pin aliases (messages) are derived from [`adafruit_hid.consumer_control_code.ConsumerControlCode`](https://docs.circuitpython.org/projects/hid/en/latest/api.html#adafruit-hid-consumer-control-code-consumercontrolcode). You can also use [this great library](https://consumer-control-extended.readthedocs.io/en/latest/index.html) for extended Consumer Control functionality. However, note that the Consumer Control messages are *not necessarily the same* between these two libraries. Check the documentation for each before proceeding.

There are also a few options that can be changed. For example, if you don't want the board led to flash, it can be disabled by setting `board_led` `enable` to `false`. Also depending on how your rotary encoder is connected it might be necessary to change `rotary_encoder` `reverse` setting to `true`, etc.

If you want to change the functionality of the rotary encoder, it must be done in [`usb_cc.py`](https://gitlab.com/c8767/diy-consumercontroller/-/blob/main/CIRCUITPY/usb_cc/usb_cc.py). There are two callbacks: `volume_up` and `volume_down`. The messages in these callbacks can be changed to match the functionality you want from either the Adafruit HID library or the Consumer Control Extended library.

Finally, in `boot.py` there is a delay that prevents the USB connection from initializing and the [`usb_cc`](https://gitlab.com/c8767/diy-consumercontroller/-/blob/main/CIRCUITPY/boot.py) app from loading for 10 seconds (I found this is necessary for my Moode Audio setup). If you find you need a different delay value, you can change `time.sleep(10)` to any value you like or delete `boot.py` altogether if your project doesn't need a boot delay.
