# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import rotaryio
import asyncio
from framework import Peripheral

class IncrementalEncoder(Peripheral):
    def set_defaults(self):
        self.sleep = 0.01
        self.divisor = 2
        self.reverse = False

        self.position = 0
        self.event = False
        self.increased = False
        self.deceased = False

        self.callbacks = {'event': [],
                          'increased': [],
                          'decreased': []}

    async def run(self):
        with rotaryio.IncrementalEncoder(*self.pins, divisor=self.divisor) as encoder:
            _position = encoder.position
            while True:
                self.position = encoder.position * {True: -1, False: 1}[self.reverse]
                if _position < self.position:
                    self._event_handler('increased')
                if _position > self.position:
                    self._event_handler('decreased')
                if _position != self.position:
                    self._event_handler('event')
                    _position = self.position
                await asyncio.sleep(self.sleep)
