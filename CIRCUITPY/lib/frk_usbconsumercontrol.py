# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import asyncio
import usb_hid
from adafruit_hid.consumer_control import ConsumerControl
from framework import Peripheral

try:
    import consumer_control_extended as ConsumerControlCode
except:
    from adafruit_hid.consumer_control_code import ConsumerControlCode


class USBConsumerControl(Peripheral):
    def set_defaults(self):
        self.sleep = 0.01
        self.message = None

        self.sent = False

        self.callbacks = {'sent': []}

    async def run(self):
        cc = ConsumerControl(usb_hid.devices)
        while True:
            if self.message is not None:
                cc.send(getattr(ConsumerControlCode, self.message, None))
                self._event_handler('sent')
                self.message = None
            await asyncio.sleep(self.sleep)
