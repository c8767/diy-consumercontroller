# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

import asyncio
from framework import Peripheral

class OneShot(Peripheral):
    def set_defaults(self):
        self.sleep = 0
        self.initial_value = False
        self.enable = True
        self.t_on = 0.1

        self.value = False
        self.start = False
        self.started = False

        self.event = False
        self.rising = False
        self.falling = False

        self.callbacks = {'event': [],
                          'rising': [],
                          'falling': []}

    def __event_handler(self):
        if self.value:
            self._event_handler('rising')
        else:
            self._event_handler('falling')
        self._event_handler('event')

    async def run(self):
        sleep = self.sleep
        self.value = self.initial_value
        _value = self.value
        while True:
            if self.enable:
                if self.started:
                    self.value = not self.value
                    self.__event_handler()
                    self.started = False
                    sleep = self.sleep
                elif self.start and not self.started:
                    self.value = not self.value
                    self.__event_handler()
                    self.started = True
                    self.start = False
                    sleep = self.t_on
                else:
                    sleep = self.sleep
            else:
                self.value = False
            await asyncio.sleep(sleep)

