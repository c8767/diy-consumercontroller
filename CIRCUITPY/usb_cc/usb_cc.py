# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT

#imports
from framework import buttons, board_led, rotary_encoder, oneshot, usb_cc

# Callbacks (run when called)
def volume_up():
    usb_cc.message = 'VOLUME_INCREMENT'

def volume_down():
    usb_cc.message = 'VOLUME_DECREMENT'

def buttons_callback():
    usb_cc.message = buttons.pin_alias

def flash_led():
    oneshot.start = True

# Setup (run once) and loop (run forever)
def setup():
    rotary_encoder.register_callback('increased', volume_up)
    rotary_encoder.register_callback('decreased', volume_down)
    buttons.register_callback('pressed', buttons_callback)
    usb_cc.register_callback('sent', flash_led)

def loop():
    board_led.value = oneshot.value
